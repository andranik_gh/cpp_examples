#include <iostream>

enum class Traffic_light { //default element type is int
	red, 
	yellow,
	green
};

enum class  Warning: char { //element type is int
	green,
	yellow,
	orange,
	red
};

std::ostream&  operator<<(std::ostream& os, Traffic_light& b)
{
	switch(b) 
	{
		case Traffic_light::red :
			os<<"Traffic_light::red";
			break;
		case Traffic_light::yellow :
			os<<"Traffic_light::yellow";
			break;
		case Traffic_light::green :
			os<<"Traffic_light::green";
			break;
	}
	return os;
}

enum class Printer_flags {
	acknowledge=1,
	paper_empty=2,
	busy=4,
	out_of_black=8,
	out_of_color=16
};

constexpr Printer_flags operator|(Printer_flags a, Printer_flags b)
{
	return static_cast<Printer_flags>(static_cast<int>(a)|static_cast<int>(b));
}

constexpr Printer_flags operator&(Printer_flags a, Printer_flags b)
{
	return static_cast<Printer_flags>(static_cast<int>(a)&static_cast<int>(b));
}

int main()
{
	Traffic_light a = Traffic_light::red;
	//The sizeof an enum class is the sizeof of its underlying type.
	std::cout<<sizeof(a)<<" "<<a<<"\n";
	Warning b;
	std::cout<<sizeof(b)<<"\n";
}
