#include <iostream>
template<typename T, typename U>
auto sum(const T& t, const U& u) -> decltype(T{} + U{})
{
	decltype(T{} + U{}) s = t + u;
	return s;
}

int main()
{
	int a = 5;
	double b = 6.5;
	std::cout<<sum(6.5, 5);
}

