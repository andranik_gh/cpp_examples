#include <iostream>

enum Lights {green, yellow, red};

Lights& operator++(Lights& t)
	// prefix increment: ++
{
	switch (t) {
		case Lights::green:
			return t=Lights::yellow;
		case Lights::yellow:
			return t=Lights::red;
		case Lights::red:
			return t=Lights::green;
	}
}
std::ostream& operator<<(std::ostream& os, Lights f)
{
	switch(f)
	{
		case Lights::red: os << "Lights::red"; return os;
		case Lights::green: os << "Lights::green"; return os;
		case Lights::yellow: os << "Lights::yellow"; return os;
	}
}
int main(){
	Lights light = Lights::red;
	std::cout<<light<<std::endl;
	Lights next = ++light;
	std::cout<<next<<std::endl;
	std::cout<<light<<std::endl;
}
