//6.1 The ISO C++ Standard

#include <iostream>
int main()
{
	unsigned char a  = 1225; // implementation-defined
	std::cout<<a<<"\n";

	int *b = new int();//unspecified;
	std::cout<<*b<<"\n";

	const int size = 4*1024;
	char page[size];
	page[size + size] = 7; //undefined behavior
	std::cout<<page[size + size]<<"\n";
	
	
}	
