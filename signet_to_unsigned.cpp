//Converting for signed to unsigned

#include <iostream>
int main()
{
	int a = -6;
	unsigned int c = a;
	std::cout << c; //2^32 - 6 = 4294967290; 
	
	unsigned int ic = 4294967290;
	int i = ic;
	std::cout << i; //2^32 - 4294967290 = -6; 
}	
