/*The int value returned by main() , if any, is the program’s return value to ‘‘the system.’’ If no value is returned, the system will receive a value indicating successful completion. A nonzero value from main() indicates failure.*/

int main() {
	//return 3809; //The exit code is 3809%256=225
	return -7; //The exit code is 249 
}

// Use echo $? command to see the exit code in linux.
